#!/usr/bin/python3

import sys
import requests
import json
from multiprocessing import cpu_count
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool

THREADS = cpu_count() > 1 and cpu_count() * 2 or 1


def get_alpha_item(item_id):
    req_params = {
        'url': 'http://alpha-admin.printi.com.br/switch/order_items/{}/2'.format(item_id),
        'params': {'token': 'c8d03ae93bca651595e89e499f89c698'}
    }
    r = requests.get(**req_params)
    r.raise_for_status()
    return r.json().get('V2', {})


def post_item_to_omega(item_info):
    item_id = item_info['data']['items'][0]['id']
    print('>> [START] Sending item #{} to OM2.'.format(item_id))
    r = requests.post(
        url='https://api-omega.printi.com/prod/kernel/v1/order/import',
        json=item_info.get('data'),
        headers=item_info.get('headers')
    )
    try:
        r.raise_for_status()
        print('>> [FINISH] Item #{} successfully sent to OM2.'.format(item_id))
    except Exception as e:
        print('>> [ERROR] Sending item #{} to OM2: {}'.format(
            item_id, r.json()))
    return r.json()


def post_item_to_omega_single_thread(items):
    pool = ThreadPool(1)
    r = pool.map(post_item_to_omega, items)
    pool.close()
    pool.join()
    return r

def post_item_to_omega_threaded(items):
    pool = ThreadPool(THREADS)
    r = pool.map(post_item_to_omega, items)
    pool.close()
    pool.join()
    return r

def get_alpha_item_threaded(item_ids):
    pool = ThreadPool(THREADS)
    r = pool.map(get_alpha_item, item_ids)
    pool.close()
    pool.join()
    return r


if __name__ == "__main__":
    item_ids = sys.argv
    item_ids.pop(0)

    try:
        items = get_alpha_item_threaded(item_ids)
        result = post_item_to_omega_threaded(items)

    except Exception as e:
        print(e)
        pass
    pass
